from django.shortcuts import render
from .serializers import ChargeSerializer, Charge
from rest_framework import viewsets


class ChargeViewSet(viewsets.ModelViewSet):

    queryset = Charge.objects.all()
    serializer_class = ChargeSerializer

# Create your views here.
