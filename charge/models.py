from datetime import date
from django.db import models
# Create your models here.

class Charge(models.Model):
    title = models.CharField(blank=True, null=True, max_length=50)
    amount = models.FloatField(blank=True, null=True)
    date = models.DateField(auto_now=True)
    firstName = models.CharField(blank=True, null=True, max_length=50)
    lastName = models.CharField(blank=True, null=True, max_length=50)

    def __str__(self):
       return "%s" % (self.title)

    class Meta:
        ordering = ['title']
