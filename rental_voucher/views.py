from django.shortcuts import render
from .serializers import RentalSerializer, Rental
from rest_framework import viewsets


class RentalViewSet(viewsets.ModelViewSet):

    queryset = Rental.objects.all()
    serializer_class = RentalSerializer

# Create your views here.
