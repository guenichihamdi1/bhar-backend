from rest_framework import serializers
from .models import Rental
from orderedItem.serializers import OrderedItemSerializer
#

class RentalSerializer(serializers.ModelSerializer):
    items_Data_2 = OrderedItemSerializer(
        source="items", many=True, read_only=True)
    class Meta:
        model = Rental
        fields = '__all__'