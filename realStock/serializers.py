from rest_framework import serializers
from .models import RealStock
#


class RealStockSerializer(serializers.ModelSerializer):
    class Meta:
        model = RealStock
        fields = '__all__'
