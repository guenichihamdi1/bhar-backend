from django.shortcuts import render
from .serializers import RealStockSerializer, RealStock
from rest_framework import viewsets


class RealStockViewSet(viewsets.ModelViewSet):

    queryset = RealStock.objects.all()
    serializer_class = RealStockSerializer

# Create your views here.
