from django.db import models
# Create your models here.

class RealStock(models.Model):
    item = models.CharField(max_length=100)
    quantity = models.IntegerField()
    unitPrice = models.FloatField()
    totalPrice = models.FloatField()
    vendor = models.CharField(max_length=100, blank=True)
    vendorTel = models.CharField(max_length=100, blank=True)
    buyDate = models.DateField(auto_now=True)

    def __str__(self):
       return "%s" % (self.item)

    class Meta:
        ordering = ['item']
