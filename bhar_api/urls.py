"""bhar_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from rest_framework import routers
from .views import UserViewSet, my_mail
from events.views import EventViewSet
from realStock.views import RealStockViewSet
from orderedItem.views import OrderedItemViewSet
from quote.views import QuoteViewSet
from invoice.views import InvoiceViewSet
from rental_voucher.views import RentalViewSet
from human_resources.views import RHViewSet
from charge.views import ChargeViewSet
from django.urls import path, include
from django.views.generic import TemplateView



router = routers.DefaultRouter()
router.register(r'events', EventViewSet)
router.register(r'users', UserViewSet)
router.register(r'realstocks', RealStockViewSet)
router.register(r'ordereditems', OrderedItemViewSet)
router.register(r'quotes', QuoteViewSet)
router.register(r'invoices', InvoiceViewSet)
router.register(r'rentalVoucher', RentalViewSet)
router.register(r'humanresources', RHViewSet)
router.register(r'charges', ChargeViewSet)
    

urlpatterns = [
    path('', include(router.urls)),
    path('email/', my_mail),
    path('authentification/', include('dj_rest_auth.urls')),
    path('authentification/registration/',include('dj_rest_auth.registration.urls')),
    path('authentification/password/reset/confirm/<uidb64>/<token>/', TemplateView.as_view(), name='password_reset_confirm'),
    path('admin/', admin.site.urls),
    path('authentification/password/reset/verify/email', include('django_rest_passwordreset.urls', namespace='password_reset')),
]
