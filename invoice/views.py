from django.shortcuts import render
from .serializers import InvoiceSerializer, Invoice
from rest_framework import viewsets


class InvoiceViewSet(viewsets.ModelViewSet):

    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    filterset_fields = ['invoice_status']

# Create your views here.
