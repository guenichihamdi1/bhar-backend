from django.db import models
from orderedItem.models import OrderedItem
# Create your models here.
choices = (
    ('En attente', 'En attente'),
    ('Confirmée', 'Confirmée'),
    ('Annulée', 'Annulée'),
)
choicesP = (
    ('Payée', 'Payée'),
    ('Impayée', 'Impayée'),
)
class Invoice(models.Model):
    number = models.IntegerField(blank=True, null=True)
    items = models.ManyToManyField(OrderedItem, related_name="items_Data")
    date = models.DateField(auto_now=True)
    invoice_status = models.CharField(blank=True, null=True, choices=choices, max_length=50)
    client_name = models.CharField(max_length=50, blank=True, null=True)
    client_adr = models.CharField(max_length=200, blank=True, null=True)
    client_tel = models.CharField(max_length=50, blank=True, null=True)
    client_mf = models.CharField(max_length=50, blank=True, null=True)
    invoice_date = models.DateField(blank=True, null=True)
    tva = models.FloatField(blank=True, null=True)
    fiscal = models.FloatField(blank=True, null=True)
    total_ttc = models.FloatField(blank=True, null=True)
    total_net_ht = models.FloatField(blank=True, null=True)
    payment_status = models.CharField(blank=True, null=True, choices=choicesP, max_length=50)
    advance = models.FloatField(blank=True, null=True)
    rest = models.FloatField(blank=True, null=True)

    def __str__(self):
       return "%s" % (self.number)

    class Meta:
        ordering = ['number']
