from rest_framework import serializers
from .models import Invoice
from orderedItem.serializers import OrderedItemSerializer
#

class InvoiceSerializer(serializers.ModelSerializer):
    items_Data = OrderedItemSerializer(
        source="items", many=True, read_only=True)
    class Meta:
        model = Invoice
        fields = '__all__'