from django.db import models
# Create your models here.
choices = (
    ('Professionnel', 'Professionnel'),
    ('Mariage', 'Mariage'),
    ('Fiançailles', 'Fiançailles'),
    ('Circoncision', 'Circoncision'),
    ('Contrat', 'Contrat'),
)
class Event(models.Model):
    title = models.CharField(max_length=100)
    client = models.CharField(max_length=100)
    local = models.CharField(max_length=100)
    classes = models.CharField(max_length=100)
    label = models.CharField(choices=choices, max_length=50)
    startDate = models.DateField()
    endDate = models.DateField()
    comment = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
       return "%s" % (self.title)
    class Meta:
        ordering = ['title']
