from django.shortcuts import render
from .serializers import EventSerializer, Event
from rest_framework import viewsets



class EventViewSet(viewsets.ModelViewSet):

    queryset = Event.objects.all()
    serializer_class = EventSerializer

# Create your views here.
