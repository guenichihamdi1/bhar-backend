from django.shortcuts import render
from .serializers import OrderedItemSerializer, OrderedItem
from rest_framework import viewsets


class OrderedItemViewSet(viewsets.ModelViewSet):

    queryset = OrderedItem.objects.all()
    serializer_class = OrderedItemSerializer
    filterset_fields = ['status']

# Create your views here.
