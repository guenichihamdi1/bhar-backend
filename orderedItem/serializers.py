from rest_framework import serializers
from .models import OrderedItem, RealStock
from realStock.serializers import RealStockSerializer
#

class OrderedItemSerializer(serializers.ModelSerializer):
    orderedItemData = RealStockSerializer(source="orderedItem", many=False, read_only=True)
    class Meta:
        model = OrderedItem
        fields = '__all__'