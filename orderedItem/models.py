from django.db import models
from realStock.models import RealStock
# Create your models here.

choices = (
    ('ordered', 'ordered'),
    ('not_ordered', 'not_ordered'),
)

class OrderedItem(models.Model):
    orderedItem = models.ForeignKey(RealStock, related_name="orderedItemData", on_delete=models.CASCADE)
    status = models.CharField(blank=True, null=True, choices=choices, max_length=50)
    quantity = models.IntegerField()
    unitPrice = models.FloatField()
    totalPrice = models.FloatField()
    ordereStartdDate = models.DateField(auto_now=False, auto_now_add=False, null= True)
    orderedEndDate = models.DateField(auto_now=False, auto_now_add=False, null= True)

    def __str__(self):
       return "%s" % (self.orderedItem)
    class Meta:
        ordering = ['orderedItem']
