from django.shortcuts import render
from .serializers import QuoteSerializer, Quote
from rest_framework import viewsets


class QuoteViewSet(viewsets.ModelViewSet):

    queryset = Quote.objects.all()
    serializer_class = QuoteSerializer

# Create your views here.
