from django.db import models
from orderedItem.models import OrderedItem
# Create your models here.
choices = (
    ('En attente', 'En attente'),
    ('Confirmé', 'Confirmé'),
    ('Annulé', 'Annulé'),
)

class Quote(models.Model):
    number = models.IntegerField(blank=True, null=True)
    items = models.ManyToManyField(OrderedItem, related_name="itemsData")
    date = models.DateField(auto_now=True)
    quote_status = models.CharField(blank=True, null=True, choices=choices, max_length=50)
    client_name = models.CharField(max_length=50, blank=True, null=True)
    client_adr = models.CharField(max_length=200, blank=True, null=True)
    client_tel = models.CharField(max_length=50, blank=True, null=True)
    start_date = models.DateField( blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    total_ht = models.FloatField(blank=True, null=True)
    total_net_ht = models.FloatField(blank=True, null=True)
    advance = models.FloatField(blank=True, null=True)
    rest = models.FloatField(blank=True, null=True)

    def __str__(self):
       return "%s" % (self.number)

    class Meta:
        ordering = ['number']
