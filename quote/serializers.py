from rest_framework import serializers
from .models import Quote
from orderedItem.serializers import OrderedItemSerializer
#

class QuoteSerializer(serializers.ModelSerializer):
    itemsData = OrderedItemSerializer(source="items",many=True, read_only=True)
    class Meta:
        model = Quote
        fields = '__all__'