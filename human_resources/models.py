from django.db import models
from realStock.models import RealStock
# Create your models here.

choices = (
    ('Employé', 'Employé'),
)

class RH(models.Model):
    firstName = models.CharField(blank=True, null=True, max_length=50)
    lastName = models.CharField(blank=True, null=True, max_length=100)
    status = models.CharField(choices=choices, max_length=50)
    adr = models.CharField(blank=True, null=True, max_length=100)
    telephone = models.CharField(blank=True, null=True, max_length=100)
    subsidiary = models.CharField(blank=True, null=True, max_length=100)
    salary = models.FloatField(blank=True, null=True)
    cin = models.IntegerField(blank=True, null=True)

    def __str__(self):
       return "%s" % (self.firstName)
    class Meta:
        ordering = ['firstName']
