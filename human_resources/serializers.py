from rest_framework import serializers
from .models import RH

class RHSerializer(serializers.ModelSerializer):
    class Meta:
        model = RH
        fields = '__all__'