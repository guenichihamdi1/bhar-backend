from django.shortcuts import render
from .serializers import RHSerializer, RH
from rest_framework import viewsets


class RHViewSet(viewsets.ModelViewSet):

    queryset = RH.objects.all()
    serializer_class = RHSerializer

# Create your views here.
    